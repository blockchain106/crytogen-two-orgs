
#Org1
echo "====> Update Anchor Peer Org1"
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_MSPID="Org1MSP"

export CORE_PEER_ADDRESS=localhost:7051

peer channel update \
	-o localhost:7050 \
	-c mychannel \
	-f ./artifacts/Org1Anchor.tx


#Org2
echo "====> Update Anchor Peer Org2"
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_MSPID="Org2MSP"

export CORE_PEER_ADDRESS=localhost:8052

peer channel update \
	-o localhost:7050 \
	-c mychannel \
	-f ./artifacts/Org2Anchor.tx