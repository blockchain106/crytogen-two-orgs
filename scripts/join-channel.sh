#export binary
export PATH=${PWD}/bin:$PATH

#Org1
echo "====> Org1 joining channel"
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_MSPID="Org1MSP"

export CORE_PEER_ADDRESS=localhost:7051

peer channel join -b ./mychannel.block

#Org2
echo "====> Org2 joining channel"
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_MSPID="Org2MSP"

export CORE_PEER_ADDRESS=localhost:8052

peer channel join -b ./mychannel.block

peer channel list