

export PATH=${PWD}/bin:$PATH

ORDERER_ADDRESS="localhost:7050"

# All config stored under the config folder
export CONFIG_DIRECTORY=${PWD}/config
export FABRIC_CFG_PATH=${PWD}/config

# Variables for setting peer addresses
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_MSPID="Org2MSP"
export CORE_PEER_LISTENADDRESS=0.0.0.0:8052
export CORE_PEER_ADDRESS=localhost:8052
export CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:10052
export CORE_PEER_CHAINCODEADDRESS=localhost:10052
export CORE_PEER_GOSSIP_BOOTSTRAP=localhost:9051
export CORE_PEER_GOSSIP_EXTERNALENDPOINT=localhost:9051
export CORE_OPERATIONS_LISTENADDRESS=localhost:10444

# Change this to folder for managing the ledger
# You may use the following to point to the current folder
# Be aware that GoLevelDB does not work well with mounted file systems so you may see
# errors in using the folder that is on host system.
export CORE_PEER_FILESYSTEMPATH=${PWD}/ledger-org2/
export CORE_PEER_ROOTDIR=${PWD}/ledger-org2/ledgersData/pvtdataStore

# Identity set to Admin
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/msp/

# State Data persistence in CouchDB
export CORE_LEDGER_STATE_STATEDATABASE=CouchDB
# For Couch DB
export CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=localhost:6984
export CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin
export CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password

#6 Launch the peer in background
peer node start